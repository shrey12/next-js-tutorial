import Link from "next/link";
import styles from "../styles/Home.module.css";
import Head from "next/head";
export default function Home() {
  return (
    <>
      <Head>
        <title>Listings | Home</title>
        <meta name="keywords" content="listings" />
      </Head>
      <div>
        <h1 className={styles.title}>Home</h1>
        <p className={styles.text}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
          malesuada nisl justo, blandit tincidunt diam aliquam ac. Nam eleifend
          auctor ligula, vitae ornare ante cursus sed. In mollis tempus arcu sit
          amet ultrices. Mauris dignissim convallis lacus, sed pellentesque
          tortor vestibulum id. Curabitur condimentum augue eu venenatis
          efficitur. Pellentesque habitant morbi tristique senectus et netus et
          malesuada fames ac turpis egestas. Proin id turpis dui. Mauris in
          aliquet diam, vel pretium risus. Fusce porta libero quis dolor
          imperdiet, quis cursus ante porttitor. Nulla sagittis ipsum vel enim
          blandit luctus. Suspendisse potenti. Ut dapibus mi sed justo vulputate
          iaculis. Vivamus facilisis ligula in blandit viverra. Nulla molestie
          tempor faucibus. Pellentesque ac leo at eros pulvinar tincidunt. Nam
          feugiat odio in lacus fermentum scelerisque. Nullam et vulputate
          dolor, at iaculis magna. Donec maximus tristique dictum. Maecenas
          dapibus malesuada magna, a scelerisque neque pretium id. Ut ac porta
          augue.
        </p>
        <p className={styles.text}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
          malesuada nisl justo, blandit tincidunt diam aliquam ac. Nam eleifend
          auctor ligula, vitae ornare ante cursus sed. In mollis tempus arcu sit
          amet ultrices. Mauris dignissim convallis lacus, sed pellentesque
          tortor vestibulum id. Curabitur condimentum augue eu venenatis
          efficitur. Pellentesque habitant morbi tristique senectus et netus et
          malesuada fames ac turpis egestas. Proin id turpis dui. Mauris in
          aliquet diam, vel pretium risus. Fusce porta libero quis dolor
          imperdiet, quis cursus ante porttitor. Nulla sagittis ipsum vel enim
          blandit luctus. Suspendisse potenti. Ut dapibus mi sed justo vulputate
          iaculis. Vivamus facilisis ligula in blandit viverra. Nulla molestie
          tempor faucibus. Pellentesque ac leo at eros pulvinar tincidunt. Nam
          feugiat odio in lacus fermentum scelerisque. Nullam et vulputate
          dolor, at iaculis magna. Donec maximus tristique dictum. Maecenas
          dapibus malesuada magna, a scelerisque neque pretium id. Ut ac porta
          augue.
        </p>
        <Link href="/other">
          <a className={styles.btn}>See Other Listing</a>
        </Link>
      </div>
    </>
  );
}
