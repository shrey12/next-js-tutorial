import Head from "next/head";

const About = () => {
  return (
    <>
      <Head>
        <title>Listings | Home</title>
        <meta name="keywords" content="listings" />
      </Head>
      <div>
        <h1>About</h1>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
          malesuada nisl justo, blandit tincidunt diam aliquam ac. Nam eleifend
          auctor ligula, vitae ornare ante cursus sed. In mollis tempus arcu sit
          amet ultrices. Mauris dignissim convallis lacus, sed pellentesque
          tortor vestibulum id. Curabitur condimentum augue eu venenatis
          efficitur. Pellentesque habitant morbi tristique senectus et netus et
          malesuada fames ac turpis egestas. Proin id turpis dui. Mauris in
          aliquet diam, vel pretium risus. Fusce porta libero quis dolor
          imperdiet, quis cursus ante porttitor. Nulla sagittis ipsum vel enim
          blandit luctus. Suspendisse potenti. Ut dapibus mi sed justo vulputate
          iaculis. Vivamus facilisis ligula in blandit viverra. Nulla molestie
          tempor faucibus. Pellentesque ac leo at eros pulvinar tincidunt. Nam
          feugiat odio in lacus fermentum scelerisque. Nullam et vulputate
          dolor, at iaculis magna. Donec maximus tristique dictum. Maecenas
          dapibus malesuada magna, a scelerisque neque pretium id. Ut ac porta
          augue.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
          malesuada nisl justo, blandit tincidunt diam aliquam ac. Nam eleifend
          auctor ligula, vitae ornare ante cursus sed. In mollis tempus arcu sit
          amet ultrices. Mauris dignissim convallis lacus, sed pellentesque
          tortor vestibulum id. Curabitur condimentum augue eu venenatis
          efficitur. Pellentesque habitant morbi tristique senectus et netus et
          malesuada fames ac turpis egestas. Proin id turpis dui. Mauris in
          aliquet diam, vel pretium risus. Fusce porta libero quis dolor
          imperdiet, quis cursus ante porttitor. Nulla sagittis ipsum vel enim
          blandit luctus. Suspendisse potenti. Ut dapibus mi sed justo vulputate
          iaculis. Vivamus facilisis ligula in blandit viverra. Nulla molestie
          tempor faucibus. Pellentesque ac leo at eros pulvinar tincidunt. Nam
          feugiat odio in lacus fermentum scelerisque. Nullam et vulputate
          dolor, at iaculis magna. Donec maximus tristique dictum. Maecenas
          dapibus malesuada magna, a scelerisque neque pretium id. Ut ac porta
          augue.
        </p>
      </div>
    </>
  );
};

export default About;
